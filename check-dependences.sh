#!/bin/bash

DEPFILE="dependences.txt"
errorFound=0

if [ ! -f $DEPFILE ]
then
    echo "$DEPFILE not found" >&2
    exit 2
fi

for i in $(cat $DEPFILE)
do
    if type $i &>/dev/null
    then
        echo "$i found"
    else
        echo "ERROR: $i not found"
        errorFound=1
    fi
done

exit $errorFound
