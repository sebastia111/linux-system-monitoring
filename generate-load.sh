#!/bin/bash

##########
# README #
##########
# sudo setcap "cap_net_admin,cap_net_raw+pe" /usr/local/sbin/nethogs
# to use nethogs without sudo

#arrayToRegex() {
#    echo "fdjshfadshkl"
#}

cloudUrl=$(curl https://my.pcloud.com/publink/show?code=XZ3LcNZcdnnLUpwvFRk0huW8k8EeV7zSmwk 2>/dev/null |
                    grep downloadlink |
                    sed -E "s/.*\:\ \"|\",//g" |
                    sed -E 's/\\\//\//g')

if [[ $linkToPcloud == *";"* || $linkToPcloud == *"\""* || $linkToPcloud == *"\|"* || $linkToPcloud == *"&&"* ]]
then
    echo "Link may not be secure to use in eval: ${linkToPcloud}" >&2
    exit -1
fi

hddPath="~/ubuntu-16.04.2-desktop-amd64.iso"
usbPath="/run/media/sebass/3C09-23E8/ubuntu-16.04.2-desktop-amd64.iso"
#localUrl="http://192.168.1.99:8000/ubuntu-16.04.2-desktop-amd64.iso"
localUrl="https://trex.tk/ubuntu-16.04.2-desktop-amd64.iso"
#serverUrl="http://releases.ubuntu.com/16.04.2/ubuntu-16.04.2-desktop-amd64.iso"
serverUrl="http://ftp.rediris.es/mirror/ubuntu-releases/16.04/ubuntu-16.04.2-desktop-amd64.iso"
baseName="ubuntu.iso"

COMMANDS=(
"cp ${hddPath} "
"cp ${usbPath} "
"wget \"${localUrl}\" -O "
"wget \"${serverUrl}\" -O "
"wget \"${cloudUrl}\" -O "
)
NAME=(hd usb wifi server cloud)

folder=logs-$(date +%F-%H-%M-%S)
mkdir $folder

for i in ${!COMMANDS[@]}; do
    load=${COMMANDS[$i]}
    loadName=${NAME[$i]}

    for j in {1..4}; do
        sudo sysctl -w vm.drop_caches=3
        pids=()
        files=()
        fileName="$loadName$j$baseName"
        for (( k=0; k < j ; k++ )); do
            tmpFile=$fileName-$k
            { time ( eval "${load} ${tmpFile} &"
                   pid=$!
                   echo $pid >> ${folder}/$fileName-pids.txt
                   wait $pid ) } 2>> ${folder}/time-$tmpFile.txt &
            pids[$k]=$!
            echo ${pids[$k]}
            files[$k]=$tmpFile
        done
        #echo ${pids[@]} > $folder/PIDS-$fileName.txt

        #pidstat -d 2 > pidstat-$fileName.txt &
        #monitorpids[$k]=$!

        top -b -d 2 > ${folder}/top-$fileName.txt &
        monitorpids[0]=$!

        nethogs -d 2 -t > ${folder}/nethogs-$fileName.txt &
        monitorpids[1]=$!

        sudo iotop -b -d 2 > ${folder}/iotop-$fileName.txt &
        monitorpids[2]=$!

        wait ${pids[@]}
        sudo kill ${monitorpids[@]}

        rm ${files[@]}
    done
done
